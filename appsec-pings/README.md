These rules are for things AppSec would like to review but don't require a finding in the Vulnerability Report page.

See also the FAQ in the [parent directory's README](https://gitlab.com/gitlab-com/gl-security/appsec/sast-custom-rules/-/tree/main/#faq)
