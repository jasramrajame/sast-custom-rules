# ruleid: glappsec_dangerous_html_safe
"<a href='#{PROMOTION_URL}' #{normal_var} target='_blank' rel='noopener noreferrer'>".html_safe

# ruleid: glappsec_dangerous_html_safe
"<a href='#{PROMOTION_URL}' \" #{var} variable after the escaped quote".html_safe

# ruleid: glappsec_dangerous_html_safe
"<a href='#{normal_var}' target='_blank' rel='noopener noreferrer'>".html_safe

# ok: glappsec_dangerous_html_safe
"<a href='#{PROMOTION_URL}' target='_blank' rel='noopener noreferrer'>".html_safe

# ruleid: glappsec_dangerous_html_safe
variable.html_safe

# ok: glappsec_dangerous_html_safe
Sanitizer.sanitize(html).html_safe

sanitized = Sanitizer.sanitize(html)
# ok: glappsec_dangerous_html_safe
sanitized.html_safe

# ok: glappsec_dangerous_html_safe
'hard coded string with no user input'.html_safe

# ok: glappsec_dangerous_html_safe
s_('translation of hard coded string with no user input').html_safe

# ok: glappsec_dangerous_html_safe
_('translation of hard coded string with no user input').html_safe

# ok: glappsec_dangerous_html_safe
n_('translation of hard coded string with no user input').html_safe

# ok: glappsec_dangerous_html_safe
html_escape_once(html).html_safe

# ok: glappsec_dangerous_html_safe
generate_link('hard coded string with no user input', url).html_safe

# ok: glappsec_dangerous_html_safe
generate_link(_('translation of hard coded string with no user input'), url).html_safe

# ok: glappsec_dangerous_html_safe
link_to(merge_request.to_reference, merge_request_url(merge_request), style: "font-weight: 600;color:#3777b0;text-decoration:none").html_safe
      
# ok: glappsec_dangerous_html_safe
content_tag(:img, nil, height: "24", src: 'avatar_icon_for_user(reviewer, 24, only_path: false)', style: "border-radius:12px;margin:-7px 0 -7px 3px;", width: "24", alt: "Avatar", class: "avatar").html_safe
