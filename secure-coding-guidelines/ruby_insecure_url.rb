#ruleid: insecure-url-construction
Gitlab::UrlBlocker.validate!("https://gitlab.com", allow_localhost: true)

#ok: insecure-url-construction
Gitlab::UrlBlocker.validate!("https://gitlab.com", allow_localhost: false, schemes: ['http']
)

#ruleid: insecure-url-construction
Gitlab::UrlBlocker.validate!("https://gitlab.com", allow_localhost: false) #no schemes

#ruleid: insecure-url-construction
Gitlab::UrlBlocker.validate!("https://gitlab.com", dns_rebind_protection: false, allow_localhost: false)

#ruleid: insecure-url-construction
Gitlab::UrlBlocker.validate!("https://gitlab.com", allow_localhost: true)