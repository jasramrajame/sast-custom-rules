#ok: insecure-ciphers
response = HTTParty.get('https://gitlab.com', ssl_version: :TLSv1_3, ciphers: ['TLS_AES_128_GCM_SHA256', 'TLS_AES_256_GCM_SHA384'])

#ruleid: insecure-ciphers
response = HTTParty.get('https://gitlab.com', ssl_version: :TLSv1_3, ciphers: ['TLS_RSA_WITH_RC4_128_SHA', 'TLS_AES_256_GCM_SHA384'])