def read(url)
    x = Addressable::URI.parse(url)
# ruleid: insecure-url-parsing-1   
    Net::HTTP::Get(x)

    x = URI.parse(url)
# ruleid: insecure-url-parsing-1   
    Net::HTTP::Post(x)
end

def read(url)
    x = Addressable::URI.parse(url)
# ok: insecure-url-parsing-1   
    Gitlab::HTTP.get(x)
end